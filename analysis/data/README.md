---
title: Initiation à la bioinformatique

subtitle: Données

author: Alban Mancheron

lang: fr-FR

---

Ce répertoire contient les données utilisées pour les analyses effectuées
dans le cadre de la série d'articles d'initiation à la bioinformatique
soumis à la revue GNU/Linux Magazine France.

Le fichier [`sequences.fasta`](./sequences.fasta) contient trois séquences
du virus SARS-CoV-2 récupérées sur le site du
[NCBI](https://www.ncbi.nlm.nih.gov/).

Ces trois séquences sont également disponibles dans des fichiers
individuels:

- le fichier [`nc_045512.2.fasta`](./nc_045512.2.fasta) contient la séquence
  de référence du SARS-CoV-2;

- le fichier [`mt320538.2.fasta`](./mt320538.2.fasta) contient la première
  séquence de SARS-CoV-2 prélevée eet rendue disponible en France;

- le fichier [`mw301121.1.fasta`](./mw301121.1.fasta) contient la dernière
  séquence de SARS-CoV-2 prélevée en Chine avant le 16 mars 2020, date de
  démarrage du premier confinement en France.

Le fichier [`SARS-CoV-2.gff`](./SARS-CoV-2.gff) contient les annotations
officielles de la séquence de référnce su SARS-CoV-2
(`NC_045512.2`](./nc_045512.2.fasta).

-------------------------------------------------------------------------

Copyright © 2021-2022 -- DoccY's Production

[![Licence Creative Commons](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)

Cette œuvre est mise à disposition selon les termes de la
[Licence Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/).

This work is licensed under a
[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
