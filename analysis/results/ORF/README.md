---
title: Initiation à la bioinformatique

subtitle: Résultats (analyses individuelles des ORF)

author: Alban Mancheron

lang: fr-FR

---

Ce répertoire contient les résultats obtenus lors des analyses individuelles
des ORF effectuées dans le cadre de la série d'articles d'initiation à la
bioinformatique soumis à la revue GNU/Linux Magazine France.

Aucun résultat de ce répertoire n'a été obtenu dans le cadre du premier
article.

Les fichiers suivants ont été produits dans le cadre du second article:

- le fichier [`nc_045512.2_3.fasta`](./nc_045512.2_3.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°3;

- le fichier [`nc_045512.2_16.fasta`](./nc_045512.2_16.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°16;

- le fichier [`nc_045512.2_24.fasta`](./nc_045512.2_24.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°24;

- le fichier [`nc_045512.2_36.fasta`](./nc_045512.2_36.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°36;

- le fichier [`nc_045512.2_37.fasta`](./nc_045512.2_37.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°37;

- le fichier [`nc_045512.2_43.fasta`](./nc_045512.2_43.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°43;

- le fichier [`nc_045512.2_58.fasta`](./nc_045512.2_58.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°58;

- le fichier [`nc_045512.2_80.fasta`](./nc_045512.2_80.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°80;

- le fichier [`nc_045512.2_81.fasta`](./nc_045512.2_81.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°81;

- le fichier [`nc_045512.2_82.fasta`](./nc_045512.2_82.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°82;

- le fichier [`nc_045512.2_83.fasta`](./nc_045512.2_83.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°83;

- le fichier [`nc_045512.2_94.fasta`](./nc_045512.2_94.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°94;

- le fichier [`nc_045512.2_106.fasta`](./nc_045512.2_106.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°106;

- le fichier [`nc_045512.2_122.fasta`](./nc_045512.2_122.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°122;

- le fichier [`nc_045512.2_137.fasta`](./nc_045512.2_137.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°137;

- le fichier [`nc_045512.2_139.fasta`](./nc_045512.2_139.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°139;

- le fichier [`nc_045512.2_159.fasta`](./nc_045512.2_159.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°159;

- le fichier [`nc_045512.2_160.fasta`](./nc_045512.2_160.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°160;

- le fichier [`nc_045512.2_162.fasta`](./nc_045512.2_162.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°162;

- le fichier [`nc_045512.2_167.fasta`](./nc_045512.2_167.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°167;

- le fichier [`nc_045512.2_168.fasta`](./nc_045512.2_168.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°168;

- le fichier [`nc_045512.2_169.fasta`](./nc_045512.2_169.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°169;

- le fichier [`nc_045512.2_171.fasta`](./nc_045512.2_171.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°171;

- le fichier [`nc_045512.2_172.fasta`](./nc_045512.2_172.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°172;

- le fichier [`nc_045512.2_173.fasta`](./nc_045512.2_173.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°173;

- le fichier [`nc_045512.2_174.fasta`](./nc_045512.2_174.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°174;

- le fichier [`nc_045512.2_175.fasta`](./nc_045512.2_175.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°175;

- le fichier [`nc_045512.2_176.fasta`](./nc_045512.2_176.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°176;

- le fichier [`nc_045512.2_179.fasta`](./nc_045512.2_179.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°179;

- le fichier [`nc_045512.2_180.fasta`](./nc_045512.2_180.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°180;

- le fichier [`nc_045512.2_183.fasta`](./nc_045512.2_183.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°183;

- le fichier [`nc_045512.2_187.fasta`](./nc_045512.2_187.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°187;

- le fichier [`nc_045512.2_188.fasta`](./nc_045512.2_188.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°188;

- le fichier [`nc_045512.2_189.fasta`](./nc_045512.2_189.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°189;

- le fichier [`nc_045512.2_190.fasta`](./nc_045512.2_190.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°190;

- le fichier [`nc_045512.2_191.fasta`](./nc_045512.2_191.fasta), qui
  correspond à la séquence d'acides aminés produite à partir de l'ORF n°191;

- le fichier [`nc_045512.2_81_related.gb`](./nc_045512.2_81_related.gb), qui
  correspond aux séquences similaires à la séquence
  [nc_045512.2_81.fasta](./nc_045512.2_81.fasta) retrouvées par `blast`;

- le fichier [`nc_045512.2_81_related.fasta`](./nc_045512.2_81_related.fasta),
  qui correspond aux séquences du fichier
  [`nc_045512.2_81_related.gb`](./nc_045512.2_81_related.gb) auxquelles a
  été ajoutée la séquence [nc_045512.2_81.fasta](./nc_045512.2_81.fasta);

- le fichier [`nc_045512.2_81_related.emma`](./nc_045512.2_81_related.emma),
  qui correspond à l'affichage produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_81_related.fasta`](./nc_045512.2_81_related.fasta);

- le fichier
  [`nc_045512.2_81_related.clustal`](./nc_045512.2_81_related.clustal), qui
  correspond à l'alignement produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_81_related.fasta`](./nc_045512.2_81_related.fasta);

- le fichier [`nc_045512.2_81_related.dnd`](./nc_045512.2_81_related.dnd),
  qui correspond à l'arbre de guidage produit par le *wrapper* `emma` (de la
  suite [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_81_related.fasta`](./nc_045512.2_81_related.fasta);

- le fichier
  [`nc_045512.2_81_related_plotcon.1.png`](./nc_045512.2_81_related_plotcon.1.png),
  qui correspond à la conservation observée de l'alignement (*via* une
  fenêtre glissante) produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_81_related.fasta`](./nc_045512.2_81_related.fasta);

- le fichier
  [`nc_045512.2_81_related.dnd.svg`](./nc_045512.2_81_related.dnd.svg), qui
  correspond à la représentation graphique de l'arbre de guidage produit par
  le *wrapper* `emma` (de la suite [EMBOSS](http://emboss.sourceforge.net/))
  pour le programme [ClustalW](http://www.clustal.org/omega/) à partir du
  fichier [`nc_045512.2_81_related.fasta`](./nc_045512.2_81_related.fasta).
  Cette représentation a été générée par le programme
  [`treeviewx`](https://github.com/rdmpage/treeviewx) puis convertie au
  format PNG avec l'outil `convert` du programme
  [ImageMagick](https://imagemagick.org/);

- le fichier [`nc_045512.2_81_related.txt`](./nc_045512.2_81_related.txt),
  qui correspond à la représentation en mode texte de l'arbre de guidage
  produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_81_related.fasta`](./nc_045512.2_81_related.fasta). Cette
  représentation a été générée par l'outil `newicktotxt` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot);

- le fichier [`nc_045512.2_81_related.png`](./nc_045512.2_81_related.png),
  qui correspond à la représentation graphique de l'arbre de guidage produit
  par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_81_related.fasta`](./nc_045512.2_81_related.fasta). Cette
  représentation a été générée par l'outil `newicktops` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot), puis convertie au format
  PNG avec l'outil `convert` du programme
  [ImageMagick](https://imagemagick.org/);

- le fichier
  [`nc_045512.2_81_related2.fasta`](./nc_045512.2_81_related2.fasta),
  qui correspond aux séquences du fichier
  [`nc_045512.2_81_related.gb`](./nc_045512.2_81_related.gb) dont les noms
  commencent par `AP1A_` auxquelles a été ajoutée la séquence
  [nc_045512.2_81.fasta](./nc_045512.2_81.fasta);

- le fichier [`nc_045512.2_81_related2.emma`](./nc_045512.2_81_related2.emma),
  qui correspond à l'affichage produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_81_related.fasta`](./nc_045512.2_81_related2.fasta);

- le fichier
  [`nc_045512.2_81_related2.clustal`](./nc_045512.2_81_related2.clustal),
  qui correspond à l'alignement produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_81_related2.fasta`](./nc_045512.2_81_related2.fasta);

- le fichier [`nc_045512.2_81_related2.dnd`](./nc_045512.2_81_related2.dnd),
  qui correspond à l'arbre de guidage produit par le *wrapper* `emma` (de la
  suite [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_81_related2.fasta`](./nc_045512.2_81_related2.fasta);

- le fichier
  [`nc_045512.2_81_related2_plotcon.1.png`](./nc_045512.2_81_related2_plotcon.1.png),
  qui correspond à la conservation observée de l'alignement (*via* une
  fenêtre glissante) produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_81_related2.fasta`](./nc_045512.2_81_related2.fasta);

- le fichier [`nc_045512.2_81_related2.txt`](./nc_045512.2_81_related2.txt),
  qui correspond à la représentation en mode texte de l'arbre de guidage
  produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_81_related2.fasta`](./nc_045512.2_81_related2.fasta). Cette
  représentation a été générée par l'outil `newicktotxt` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot);

- le fichier [`nc_045512.2_81_related2.png`](./nc_045512.2_81_related2.png),
  qui correspond à la représentation graphique de l'arbre de guidage produit
  par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_81_related2.fasta`](./nc_045512.2_81_related2.fasta). Cette
  représentation a été générée par l'outil `newicktops` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot), puis convertie au format
  PNG avec l'outil `convert` du programme
  [ImageMagick](https://imagemagick.org/).


Les fichiers suivants ont été produits dans le cadre du troisième article:

- le fichier [`nc_045512.2_all.fasta`](./nc_045512.2_all.fasta) qui
  correspond à l'ensemble des séquences fasta soumise à `blast`;

- le fichier [`nc_045512.2_137_related.gb`](./nc_045512.2_137_related.gb), qui
  correspond aux séquences similaires à la séquence
  [nc_045512.2_137.fasta](./nc_045512.2_137.fasta) retrouvées par `blast`;

- le fichier [`nc_045512.2_137_related.fasta`](./nc_045512.2_137_related.fasta),
  qui correspond aux séquences du fichier
  [`nc_045512.2_137_related.gb`](./nc_045512.2_137_related.gb) auxquelles a
  été ajoutée la séquence [nc_045512.2_137.fasta](./nc_045512.2_137.fasta);

- le fichier [`nc_045512.2_137_related.emma`](./nc_045512.2_137_related.emma),
  qui correspond à l'affichage produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_137_related.fasta`](./nc_045512.2_137_related.fasta);

- le fichier
  [`nc_045512.2_137_related.clustal`](./nc_045512.2_137_related.clustal), qui
  correspond à l'alignement produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_137_related.fasta`](./nc_045512.2_137_related.fasta);

- le fichier [`nc_045512.2_137_related.dnd`](./nc_045512.2_137_related.dnd),
  qui correspond à l'arbre de guidage produit par le *wrapper* `emma` (de la
  suite [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_137_related.fasta`](./nc_045512.2_137_related.fasta);

- le fichier
  [`nc_045512.2_137_related_plotcon.1.png`](./nc_045512.2_137_related_plotcon.1.png),
  qui correspond à la conservation observée de l'alignement (*via* une
  fenêtre glissante) produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_137_related.fasta`](./nc_045512.2_137_related.fasta);

- le fichier [`nc_045512.2_137_related.txt`](./nc_045512.2_137_related.txt),
  qui correspond à la représentation en mode texte de l'arbre de guidage
  produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_137_related.fasta`](./nc_045512.2_137_related.fasta). Cette
  représentation a été générée par l'outil `newicktotxt` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot);

- le fichier [`nc_045512.2_137_related.png`](./nc_045512.2_137_related.png),
  qui correspond à la représentation graphique de l'arbre de guidage produit
  par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_137_related.fasta`](./nc_045512.2_137_related.fasta). Cette
  représentation a été générée par l'outil `newicktops` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot), puis convertie au format
  PNG avec l'outil `convert` du programme
  [ImageMagick](https://imagemagick.org/);

- le fichier [`nc_045512.2_159_related.gb`](./nc_045512.2_159_related.gb), qui
  correspond aux séquences similaires à la séquence
  [nc_045512.2_159.fasta](./nc_045512.2_159.fasta) retrouvées par `blast`;

- le fichier [`nc_045512.2_159_related.fasta`](./nc_045512.2_159_related.fasta),
  qui correspond aux séquences du fichier
  [`nc_045512.2_159_related.gb`](./nc_045512.2_159_related.gb) auxquelles a
  été ajoutée la séquence [nc_045512.2_159.fasta](./nc_045512.2_159.fasta);

- le fichier [`nc_045512.2_159_related.emma`](./nc_045512.2_159_related.emma),
  qui correspond à l'affichage produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_159_related.fasta`](./nc_045512.2_159_related.fasta);

- le fichier
  [`nc_045512.2_159_related.clustal`](./nc_045512.2_159_related.clustal), qui
  correspond à l'alignement produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_159_related.fasta`](./nc_045512.2_159_related.fasta);

- le fichier [`nc_045512.2_159_related.dnd`](./nc_045512.2_159_related.dnd),
  qui correspond à l'arbre de guidage produit par le *wrapper* `emma` (de la
  suite [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_159_related.fasta`](./nc_045512.2_159_related.fasta);

- le fichier
  [`nc_045512.2_159_related_plotcon.1.png`](./nc_045512.2_159_related_plotcon.1.png),
  qui correspond à la conservation observée de l'alignement (*via* une
  fenêtre glissante) produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_159_related.fasta`](./nc_045512.2_159_related.fasta);

- le fichier [`nc_045512.2_159_related.txt`](./nc_045512.2_159_related.txt),
  qui correspond à la représentation en mode texte de l'arbre de guidage
  produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_159_related.fasta`](./nc_045512.2_159_related.fasta). Cette
  représentation a été générée par l'outil `newicktotxt` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot);

- le fichier [`nc_045512.2_159_related.png`](./nc_045512.2_159_related.png),
  qui correspond à la représentation graphique de l'arbre de guidage produit
  par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_159_related.fasta`](./nc_045512.2_159_related.fasta). Cette
  représentation a été générée par l'outil `newicktops` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot), puis convertie au format
  PNG avec l'outil `convert` du programme
  [ImageMagick](https://imagemagick.org/);

- le fichier [`nc_045512.2_167_related.gb`](./nc_045512.2_167_related.gb), qui
  correspond aux séquences similaires à la séquence
  [nc_045512.2_167.fasta](./nc_045512.2_167.fasta) retrouvées par `blast`;

- le fichier [`nc_045512.2_167_related.fasta`](./nc_045512.2_167_related.fasta),
  qui correspond aux séquences du fichier
  [`nc_045512.2_167_related.gb`](./nc_045512.2_167_related.gb) auxquelles a
  été ajoutée la séquence [nc_045512.2_167.fasta](./nc_045512.2_167.fasta);

- le fichier [`nc_045512.2_167_related.emma`](./nc_045512.2_167_related.emma),
  qui correspond à l'affichage produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_167_related.fasta`](./nc_045512.2_167_related.fasta);

- le fichier
  [`nc_045512.2_167_related.clustal`](./nc_045512.2_167_related.clustal), qui
  correspond à l'alignement produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_167_related.fasta`](./nc_045512.2_167_related.fasta);

- le fichier [`nc_045512.2_167_related.dnd`](./nc_045512.2_167_related.dnd),
  qui correspond à l'arbre de guidage produit par le *wrapper* `emma` (de la
  suite [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_167_related.fasta`](./nc_045512.2_167_related.fasta);

- le fichier
  [`nc_045512.2_167_related_plotcon.1.png`](./nc_045512.2_167_related_plotcon.1.png),
  qui correspond à la conservation observée de l'alignement (*via* une
  fenêtre glissante) produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_167_related.fasta`](./nc_045512.2_167_related.fasta);

- le fichier [`nc_045512.2_167_related.txt`](./nc_045512.2_167_related.txt),
  qui correspond à la représentation en mode texte de l'arbre de guidage
  produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_167_related.fasta`](./nc_045512.2_167_related.fasta). Cette
  représentation a été générée par l'outil `newicktotxt` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot);

- le fichier [`nc_045512.2_167_related.png`](./nc_045512.2_167_related.png),
  qui correspond à la représentation graphique de l'arbre de guidage produit
  par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_167_related.fasta`](./nc_045512.2_167_related.fasta). Cette
  représentation a été générée par l'outil `newicktops` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot), puis convertie au format
  PNG avec l'outil `convert` du programme
  [ImageMagick](https://imagemagick.org/);

- le fichier [`nc_045512.2_168_related.gb`](./nc_045512.2_168_related.gb), qui
  correspond aux séquences similaires à la séquence
  [nc_045512.2_168.fasta](./nc_045512.2_168.fasta) retrouvées par `blast`;

- le fichier [`nc_045512.2_168_related.fasta`](./nc_045512.2_168_related.fasta),
  qui correspond aux séquences du fichier
  [`nc_045512.2_168_related.gb`](./nc_045512.2_168_related.gb) auxquelles a
  été ajoutée la séquence [nc_045512.2_168.fasta](./nc_045512.2_168.fasta);

- le fichier [`nc_045512.2_168_related.emma`](./nc_045512.2_168_related.emma),
  qui correspond à l'affichage produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_168_related.fasta`](./nc_045512.2_168_related.fasta);

- le fichier
  [`nc_045512.2_168_related.clustal`](./nc_045512.2_168_related.clustal), qui
  correspond à l'alignement produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_168_related.fasta`](./nc_045512.2_168_related.fasta);

- le fichier [`nc_045512.2_168_related.dnd`](./nc_045512.2_168_related.dnd),
  qui correspond à l'arbre de guidage produit par le *wrapper* `emma` (de la
  suite [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_168_related.fasta`](./nc_045512.2_168_related.fasta);

- le fichier
  [`nc_045512.2_168_related_plotcon.1.png`](./nc_045512.2_168_related_plotcon.1.png),
  qui correspond à la conservation observée de l'alignement (*via* une
  fenêtre glissante) produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_168_related.fasta`](./nc_045512.2_168_related.fasta);

- le fichier [`nc_045512.2_168_related.txt`](./nc_045512.2_168_related.txt),
  qui correspond à la représentation en mode texte de l'arbre de guidage
  produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_168_related.fasta`](./nc_045512.2_168_related.fasta). Cette
  représentation a été générée par l'outil `newicktotxt` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot);

- le fichier [`nc_045512.2_168_related.png`](./nc_045512.2_168_related.png),
  qui correspond à la représentation graphique de l'arbre de guidage produit
  par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_168_related.fasta`](./nc_045512.2_168_related.fasta). Cette
  représentation a été générée par l'outil `newicktops` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot), puis convertie au format
  PNG avec l'outil `convert` du programme
  [ImageMagick](https://imagemagick.org/);

- le fichier [`nc_045512.2_169_related.gb`](./nc_045512.2_169_related.gb), qui
  correspond aux séquences similaires à la séquence
  [nc_045512.2_169.fasta](./nc_045512.2_169.fasta) retrouvées par `blast`;

- le fichier [`nc_045512.2_169_related.fasta`](./nc_045512.2_169_related.fasta),
  qui correspond aux séquences du fichier
  [`nc_045512.2_169_related.gb`](./nc_045512.2_169_related.gb) auxquelles a
  été ajoutée la séquence [nc_045512.2_169.fasta](./nc_045512.2_169.fasta);

- le fichier [`nc_045512.2_169_related.emma`](./nc_045512.2_169_related.emma),
  qui correspond à l'affichage produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_169_related.fasta`](./nc_045512.2_169_related.fasta);

- le fichier
  [`nc_045512.2_169_related.clustal`](./nc_045512.2_169_related.clustal), qui
  correspond à l'alignement produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_169_related.fasta`](./nc_045512.2_169_related.fasta);

- le fichier [`nc_045512.2_169_related.dnd`](./nc_045512.2_169_related.dnd),
  qui correspond à l'arbre de guidage produit par le *wrapper* `emma` (de la
  suite [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_169_related.fasta`](./nc_045512.2_169_related.fasta);

- le fichier
  [`nc_045512.2_169_related_plotcon.1.png`](./nc_045512.2_169_related_plotcon.1.png),
  qui correspond à la conservation observée de l'alignement (*via* une
  fenêtre glissante) produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_169_related.fasta`](./nc_045512.2_169_related.fasta);

- le fichier [`nc_045512.2_169_related.txt`](./nc_045512.2_169_related.txt),
  qui correspond à la représentation en mode texte de l'arbre de guidage
  produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_169_related.fasta`](./nc_045512.2_169_related.fasta). Cette
  représentation a été générée par l'outil `newicktotxt` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot);

- le fichier [`nc_045512.2_169_related.png`](./nc_045512.2_169_related.png),
  qui correspond à la représentation graphique de l'arbre de guidage produit
  par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_169_related.fasta`](./nc_045512.2_169_related.fasta). Cette
  représentation a été générée par l'outil `newicktops` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot), puis convertie au format
  PNG avec l'outil `convert` du programme
  [ImageMagick](https://imagemagick.org/);

- le fichier [`nc_045512.2_172_related.gb`](./nc_045512.2_172_related.gb), qui
  correspond aux séquences similaires à la séquence
  [nc_045512.2_172.fasta](./nc_045512.2_172.fasta) retrouvées par `blast`;

- le fichier [`nc_045512.2_172_related.fasta`](./nc_045512.2_172_related.fasta),
  qui correspond aux séquences du fichier
  [`nc_045512.2_172_related.gb`](./nc_045512.2_172_related.gb) auxquelles a
  été ajoutée la séquence [nc_045512.2_172.fasta](./nc_045512.2_172.fasta);

- le fichier [`nc_045512.2_172_related.emma`](./nc_045512.2_172_related.emma),
  qui correspond à l'affichage produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_172_related.fasta`](./nc_045512.2_172_related.fasta);

- le fichier
  [`nc_045512.2_172_related.clustal`](./nc_045512.2_172_related.clustal), qui
  correspond à l'alignement produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_172_related.fasta`](./nc_045512.2_172_related.fasta);

- le fichier [`nc_045512.2_172_related.dnd`](./nc_045512.2_172_related.dnd),
  qui correspond à l'arbre de guidage produit par le *wrapper* `emma` (de la
  suite [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_172_related.fasta`](./nc_045512.2_172_related.fasta);

- le fichier
  [`nc_045512.2_172_related_plotcon.1.png`](./nc_045512.2_172_related_plotcon.1.png),
  qui correspond à la conservation observée de l'alignement (*via* une
  fenêtre glissante) produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_172_related.fasta`](./nc_045512.2_172_related.fasta);

- le fichier [`nc_045512.2_172_related.txt`](./nc_045512.2_172_related.txt),
  qui correspond à la représentation en mode texte de l'arbre de guidage
  produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_172_related.fasta`](./nc_045512.2_172_related.fasta). Cette
  représentation a été générée par l'outil `newicktotxt` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot);

- le fichier [`nc_045512.2_172_related.png`](./nc_045512.2_172_related.png),
  qui correspond à la représentation graphique de l'arbre de guidage produit
  par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_172_related.fasta`](./nc_045512.2_172_related.fasta). Cette
  représentation a été générée par l'outil `newicktops` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot), puis convertie au format
  PNG avec l'outil `convert` du programme
  [ImageMagick](https://imagemagick.org/);

- le fichier [`nc_045512.2_173_related.gb`](./nc_045512.2_173_related.gb), qui
  correspond aux séquences similaires à la séquence
  [nc_045512.2_173.fasta](./nc_045512.2_173.fasta) retrouvées par `blast`;

- le fichier [`nc_045512.2_173_related.fasta`](./nc_045512.2_173_related.fasta),
  qui correspond aux séquences du fichier
  [`nc_045512.2_173_related.gb`](./nc_045512.2_173_related.gb) auxquelles a
  été ajoutée la séquence [nc_045512.2_173.fasta](./nc_045512.2_173.fasta);

- le fichier [`nc_045512.2_173_related.emma`](./nc_045512.2_173_related.emma),
  qui correspond à l'affichage produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_173_related.fasta`](./nc_045512.2_173_related.fasta);

- le fichier
  [`nc_045512.2_173_related.clustal`](./nc_045512.2_173_related.clustal), qui
  correspond à l'alignement produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_173_related.fasta`](./nc_045512.2_173_related.fasta);

- le fichier [`nc_045512.2_173_related.dnd`](./nc_045512.2_173_related.dnd),
  qui correspond à l'arbre de guidage produit par le *wrapper* `emma` (de la
  suite [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_173_related.fasta`](./nc_045512.2_173_related.fasta);

- le fichier
  [`nc_045512.2_173_related_plotcon.1.png`](./nc_045512.2_173_related_plotcon.1.png),
  qui correspond à la conservation observée de l'alignement (*via* une
  fenêtre glissante) produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_173_related.fasta`](./nc_045512.2_173_related.fasta);

- le fichier [`nc_045512.2_173_related.txt`](./nc_045512.2_173_related.txt),
  qui correspond à la représentation en mode texte de l'arbre de guidage
  produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_173_related.fasta`](./nc_045512.2_173_related.fasta). Cette
  représentation a été générée par l'outil `newicktotxt` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot);

- le fichier [`nc_045512.2_173_related.png`](./nc_045512.2_173_related.png),
  qui correspond à la représentation graphique de l'arbre de guidage produit
  par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_173_related.fasta`](./nc_045512.2_173_related.fasta). Cette
  représentation a été générée par l'outil `newicktops` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot), puis convertie au format
  PNG avec l'outil `convert` du programme
  [ImageMagick](https://imagemagick.org/);

- le fichier [`nc_045512.2_175_related.gb`](./nc_045512.2_175_related.gb), qui
  correspond aux séquences similaires à la séquence
  [nc_045512.2_175.fasta](./nc_045512.2_175.fasta) retrouvées par `blast`;

- le fichier [`nc_045512.2_175_related.fasta`](./nc_045512.2_175_related.fasta),
  qui correspond aux séquences du fichier
  [`nc_045512.2_175_related.gb`](./nc_045512.2_175_related.gb) auxquelles a
  été ajoutée la séquence [nc_045512.2_175.fasta](./nc_045512.2_175.fasta);

- le fichier [`nc_045512.2_175_related.emma`](./nc_045512.2_175_related.emma),
  qui correspond à l'affichage produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_175_related.fasta`](./nc_045512.2_175_related.fasta);

- le fichier
  [`nc_045512.2_175_related.clustal`](./nc_045512.2_175_related.clustal), qui
  correspond à l'alignement produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_175_related.fasta`](./nc_045512.2_175_related.fasta);

- le fichier [`nc_045512.2_175_related.dnd`](./nc_045512.2_175_related.dnd),
  qui correspond à l'arbre de guidage produit par le *wrapper* `emma` (de la
  suite [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_175_related.fasta`](./nc_045512.2_175_related.fasta);

- le fichier
  [`nc_045512.2_175_related_plotcon.1.png`](./nc_045512.2_175_related_plotcon.1.png),
  qui correspond à la conservation observée de l'alignement (*via* une
  fenêtre glissante) produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_175_related.fasta`](./nc_045512.2_175_related.fasta);

- le fichier [`nc_045512.2_175_related.txt`](./nc_045512.2_175_related.txt),
  qui correspond à la représentation en mode texte de l'arbre de guidage
  produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_175_related.fasta`](./nc_045512.2_175_related.fasta). Cette
  représentation a été générée par l'outil `newicktotxt` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot);

- le fichier [`nc_045512.2_175_related.png`](./nc_045512.2_175_related.png),
  qui correspond à la représentation graphique de l'arbre de guidage produit
  par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_175_related.fasta`](./nc_045512.2_175_related.fasta). Cette
  représentation a été générée par l'outil `newicktops` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot), puis convertie au format
  PNG avec l'outil `convert` du programme
  [ImageMagick](https://imagemagick.org/);

- le fichier [`nc_045512.2_176_related.gb`](./nc_045512.2_176_related.gb), qui
  correspond aux séquences similaires à la séquence
  [nc_045512.2_176.fasta](./nc_045512.2_176.fasta) retrouvées par `blast`;

- le fichier [`nc_045512.2_176_related.fasta`](./nc_045512.2_176_related.fasta),
  qui correspond aux séquences du fichier
  [`nc_045512.2_176_related.gb`](./nc_045512.2_176_related.gb) auxquelles a
  été ajoutée la séquence [nc_045512.2_176.fasta](./nc_045512.2_176.fasta);

- le fichier [`nc_045512.2_176_related.emma`](./nc_045512.2_176_related.emma),
  qui correspond à l'affichage produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_176_related.fasta`](./nc_045512.2_176_related.fasta);

- le fichier
  [`nc_045512.2_176_related.clustal`](./nc_045512.2_176_related.clustal), qui
  correspond à l'alignement produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_176_related.fasta`](./nc_045512.2_176_related.fasta);

- le fichier [`nc_045512.2_176_related.dnd`](./nc_045512.2_176_related.dnd),
  qui correspond à l'arbre de guidage produit par le *wrapper* `emma` (de la
  suite [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_176_related.fasta`](./nc_045512.2_176_related.fasta);

- le fichier
  [`nc_045512.2_176_related_plotcon.1.png`](./nc_045512.2_176_related_plotcon.1.png),
  qui correspond à la conservation observée de l'alignement (*via* une
  fenêtre glissante) produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_176_related.fasta`](./nc_045512.2_176_related.fasta);

- le fichier [`nc_045512.2_176_related.txt`](./nc_045512.2_176_related.txt),
  qui correspond à la représentation en mode texte de l'arbre de guidage
  produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_176_related.fasta`](./nc_045512.2_176_related.fasta). Cette
  représentation a été générée par l'outil `newicktotxt` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot);

- le fichier [`nc_045512.2_176_related.png`](./nc_045512.2_176_related.png),
  qui correspond à la représentation graphique de l'arbre de guidage produit
  par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_176_related.fasta`](./nc_045512.2_176_related.fasta). Cette
  représentation a été générée par l'outil `newicktops` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot), puis convertie au format
  PNG avec l'outil `convert` du programme
  [ImageMagick](https://imagemagick.org/);

- le fichier [`nc_045512.2_179_related.gb`](./nc_045512.2_179_related.gb), qui
  correspond aux séquences similaires à la séquence
  [nc_045512.2_179.fasta](./nc_045512.2_179.fasta) retrouvées par `blast`;

- le fichier [`nc_045512.2_179_related.fasta`](./nc_045512.2_179_related.fasta),
  qui correspond aux séquences du fichier
  [`nc_045512.2_179_related.gb`](./nc_045512.2_179_related.gb) auxquelles a
  été ajoutée la séquence [nc_045512.2_179.fasta](./nc_045512.2_179.fasta);

- le fichier [`nc_045512.2_179_related.emma`](./nc_045512.2_179_related.emma),
  qui correspond à l'affichage produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_179_related.fasta`](./nc_045512.2_179_related.fasta);

- le fichier
  [`nc_045512.2_179_related.clustal`](./nc_045512.2_179_related.clustal), qui
  correspond à l'alignement produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_179_related.fasta`](./nc_045512.2_179_related.fasta);

- le fichier [`nc_045512.2_179_related.dnd`](./nc_045512.2_179_related.dnd),
  qui correspond à l'arbre de guidage produit par le *wrapper* `emma` (de la
  suite [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_179_related.fasta`](./nc_045512.2_179_related.fasta);

- le fichier
  [`nc_045512.2_179_related_plotcon.1.png`](./nc_045512.2_179_related_plotcon.1.png),
  qui correspond à la conservation observée de l'alignement (*via* une
  fenêtre glissante) produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_179_related.fasta`](./nc_045512.2_179_related.fasta);

- le fichier [`nc_045512.2_179_related.txt`](./nc_045512.2_179_related.txt),
  qui correspond à la représentation en mode texte de l'arbre de guidage
  produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_179_related.fasta`](./nc_045512.2_179_related.fasta). Cette
  représentation a été générée par l'outil `newicktotxt` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot);

- le fichier [`nc_045512.2_179_related.png`](./nc_045512.2_179_related.png),
  qui correspond à la représentation graphique de l'arbre de guidage produit
  par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_179_related.fasta`](./nc_045512.2_179_related.fasta). Cette
  représentation a été générée par l'outil `newicktops` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot), puis convertie au format
  PNG avec l'outil `convert` du programme
  [ImageMagick](https://imagemagick.org/);

- le fichier [`nc_045512.2_180_related.gb`](./nc_045512.2_180_related.gb), qui
  correspond aux séquences similaires à la séquence
  [nc_045512.2_180.fasta](./nc_045512.2_180.fasta) retrouvées par `blast`;

- le fichier [`nc_045512.2_180_related.fasta`](./nc_045512.2_180_related.fasta),
  qui correspond aux séquences du fichier
  [`nc_045512.2_180_related.gb`](./nc_045512.2_180_related.gb) auxquelles a
  été ajoutée la séquence [nc_045512.2_180.fasta](./nc_045512.2_180.fasta);

- le fichier [`nc_045512.2_180_related.emma`](./nc_045512.2_180_related.emma),
  qui correspond à l'affichage produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_180_related.fasta`](./nc_045512.2_180_related.fasta);

- le fichier
  [`nc_045512.2_180_related.clustal`](./nc_045512.2_180_related.clustal), qui
  correspond à l'alignement produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_180_related.fasta`](./nc_045512.2_180_related.fasta);

- le fichier [`nc_045512.2_180_related.dnd`](./nc_045512.2_180_related.dnd),
  qui correspond à l'arbre de guidage produit par le *wrapper* `emma` (de la
  suite [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_180_related.fasta`](./nc_045512.2_180_related.fasta);

- le fichier
  [`nc_045512.2_180_related_plotcon.1.png`](./nc_045512.2_180_related_plotcon.1.png),
  qui correspond à la conservation observée de l'alignement (*via* une
  fenêtre glissante) produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_180_related.fasta`](./nc_045512.2_180_related.fasta);

- le fichier [`nc_045512.2_180_related.txt`](./nc_045512.2_180_related.txt),
  qui correspond à la représentation en mode texte de l'arbre de guidage
  produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_180_related.fasta`](./nc_045512.2_180_related.fasta). Cette
  représentation a été générée par l'outil `newicktotxt` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot);

- le fichier [`nc_045512.2_180_related.png`](./nc_045512.2_180_related.png),
  qui correspond à la représentation graphique de l'arbre de guidage produit
  par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_180_related.fasta`](./nc_045512.2_180_related.fasta). Cette
  représentation a été générée par l'outil `newicktops` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot), puis convertie au format
  PNG avec l'outil `convert` du programme
  [ImageMagick](https://imagemagick.org/);

- le fichier [`nc_045512.2_183_related.gb`](./nc_045512.2_183_related.gb), qui
  correspond aux séquences similaires à la séquence
  [nc_045512.2_183.fasta](./nc_045512.2_183.fasta) retrouvées par `blast`;

- le fichier [`nc_045512.2_183_related.fasta`](./nc_045512.2_183_related.fasta),
  qui correspond aux séquences du fichier
  [`nc_045512.2_183_related.gb`](./nc_045512.2_183_related.gb) auxquelles a
  été ajoutée la séquence [nc_045512.2_183.fasta](./nc_045512.2_183.fasta);

- le fichier [`nc_045512.2_183_related.emma`](./nc_045512.2_183_related.emma),
  qui correspond à l'affichage produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_183_related.fasta`](./nc_045512.2_183_related.fasta);

- le fichier
  [`nc_045512.2_183_related.clustal`](./nc_045512.2_183_related.clustal), qui
  correspond à l'alignement produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_183_related.fasta`](./nc_045512.2_183_related.fasta);

- le fichier [`nc_045512.2_183_related.dnd`](./nc_045512.2_183_related.dnd),
  qui correspond à l'arbre de guidage produit par le *wrapper* `emma` (de la
  suite [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_183_related.fasta`](./nc_045512.2_183_related.fasta);

- le fichier
  [`nc_045512.2_183_related_plotcon.1.png`](./nc_045512.2_183_related_plotcon.1.png),
  qui correspond à la conservation observée de l'alignement (*via* une
  fenêtre glissante) produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_183_related.fasta`](./nc_045512.2_183_related.fasta);

- le fichier [`nc_045512.2_183_related.txt`](./nc_045512.2_183_related.txt),
  qui correspond à la représentation en mode texte de l'arbre de guidage
  produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_183_related.fasta`](./nc_045512.2_183_related.fasta). Cette
  représentation a été générée par l'outil `newicktotxt` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot);

- le fichier [`nc_045512.2_183_related.png`](./nc_045512.2_183_related.png),
  qui correspond à la représentation graphique de l'arbre de guidage produit
  par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_183_related.fasta`](./nc_045512.2_183_related.fasta). Cette
  représentation a été générée par l'outil `newicktops` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot), puis convertie au format
  PNG avec l'outil `convert` du programme
  [ImageMagick](https://imagemagick.org/);

- le fichier [`nc_045512.2_187_related.gb`](./nc_045512.2_187_related.gb), qui
  correspond aux séquences similaires à la séquence
  [nc_045512.2_187.fasta](./nc_045512.2_187.fasta) retrouvées par `blast`;

- le fichier [`nc_045512.2_187_related.fasta`](./nc_045512.2_187_related.fasta),
  qui correspond aux séquences du fichier
  [`nc_045512.2_187_related.gb`](./nc_045512.2_187_related.gb) auxquelles a
  été ajoutée la séquence [nc_045512.2_187.fasta](./nc_045512.2_187.fasta);

- le fichier [`nc_045512.2_187_related.emma`](./nc_045512.2_187_related.emma),
  qui correspond à l'affichage produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_187_related.fasta`](./nc_045512.2_187_related.fasta);

- le fichier
  [`nc_045512.2_187_related.clustal`](./nc_045512.2_187_related.clustal), qui
  correspond à l'alignement produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_187_related.fasta`](./nc_045512.2_187_related.fasta);

- le fichier [`nc_045512.2_187_related.dnd`](./nc_045512.2_187_related.dnd),
  qui correspond à l'arbre de guidage produit par le *wrapper* `emma` (de la
  suite [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_187_related.fasta`](./nc_045512.2_187_related.fasta);

- le fichier
  [`nc_045512.2_187_related_plotcon.1.png`](./nc_045512.2_187_related_plotcon.1.png),
  qui correspond à la conservation observée de l'alignement (*via* une
  fenêtre glissante) produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_187_related.fasta`](./nc_045512.2_187_related.fasta);

- le fichier [`nc_045512.2_187_related.txt`](./nc_045512.2_187_related.txt),
  qui correspond à la représentation en mode texte de l'arbre de guidage
  produit par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_187_related.fasta`](./nc_045512.2_187_related.fasta). Cette
  représentation a été générée par l'outil `newicktotxt` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot);

- le fichier [`nc_045512.2_187_related.png`](./nc_045512.2_187_related.png),
  qui correspond à la représentation graphique de l'arbre de guidage produit
  par le *wrapper* `emma` (de la suite
  [EMBOSS](http://emboss.sourceforge.net/)) pour le programme
  [ClustalW](http://www.clustal.org/omega/) à partir du fichier
  [`nc_045512.2_187_related.fasta`](./nc_045512.2_187_related.fasta). Cette
  représentation a été générée par l'outil `newicktops` du programme
  [`njplot`](http://doua.prabi.fr/software/njplot), puis convertie au format
  PNG avec l'outil `convert` du programme
  [ImageMagick](https://imagemagick.org/).

-------------------------------------------------------------------------

Copyright © 2021-2022 -- DoccY's Production

[![Licence Creative Commons](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)

Cette œuvre est mise à disposition selon les termes de la
[Licence Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/).

This work is licensed under a
[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
