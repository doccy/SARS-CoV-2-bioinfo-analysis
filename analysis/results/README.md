---
title: Initiation à la bioinformatique

subtitle: Résultats

author: Alban Mancheron

lang: fr-FR

---

Ce répertoire contient les résultats obtenus lors des analyses effectuées
dans le cadre de la série d'articles d'initiation à la bioinformatique
soumis à la revue GNU/Linux Magazine France.

Afin de faciliter l'exploration des fichiers, les fichiers relatifs aux
études individuelles des ORF sont placés dans le sous-répertoire
[ORF/](./ORF/).

Les résultats présentés dans le premier article sont (dans l'ordre
de présentation dans l'article) :

- le fichier
  [`nc_045512.2-mt320538.2.needle`](./nc_045512.2-mt320538.2.needle), qui
  correspond à l'alignement global entre les deux séquences
  [`NC_045512.2`](../data/nc_045512.2.fasta) et
  [`MT320538.2`](../data/mt320538.2.fasta) avec le programme `needle`;

- le fichier
  [`nc_045512.2-mt320538.2.stretcher`](./nc_045512.2-mt320538.2.stretcher),
  qui correspond à l'alignement global entre les deux séquences
  [`NC_045512.2`](../data/nc_045512.2.fasta) et
  [`MT320538.2`](../data/mt320538.2.fasta) avec le programme `stretcher`
  (sortie par défaut);

- le fichier
  [`nc_045512.2-mt320538.2.stretcher-markx1`](./nc_045512.2-mt320538.2.stretcher-markx1),
  qui correspond à l'alignement global entre les deux séquences
  [`NC_045512.2`](../data/nc_045512.2.fasta) et
  [`MT320538.2`](../data/mt320538.2.fasta) avec le programme `stretcher`
  (sortie au format `markx1`);

- le fichier
  [`nc_045512.2-mt320538.2.stretcher-markx2`](./nc_045512.2-mt320538.2.stretcher-markx2),
  qui correspond à l'alignement global entre les deux séquences
  [`NC_045512.2`](../data/nc_045512.2.fasta) et
  [`MT320538.2`](../data/mt320538.2.fasta) avec le programme `stretcher`
  (sortie au format `markx2`);

- le fichier
  [`nc_045512.2-mt320538.2.diffseq`](./nc_045512.2-mt320538.2.diffseq), qui
  correspond au calcul des différences entre les séquences
  [`NC_045512.2`](../data/nc_045512.2.fasta) et
  [`MT320538.2`](../data/mt320538.2.fasta) avec le programme `diffseq`;

- le fichier [`nc_045512.2.diffgff`](./nc_045512.2.diffgff), qui correspond
  au fichier GFF associé au fichier [`NC_045512.2`](../data/nc_045512.2.fasta)
  contenant les annotation des différences avec la séquence
  [`MT320538.2`](../data/mt320538.2.fasta) (ce fichier fait partie des
  sorties produites par le programme `diffseq`);

- le fichier [`mt320538.2.diffgff`](./mt320538.2.diffgff), qui correspond
  au fichier GFF associé au fichier [`MT320538.2`](../data/mt320538.2.fasta)
  contenant les annotation des différences avec la séquence
  [`NC_045512.2`](../data/nc_045512.2.fasta) (ce fichier fait partie des
  sorties produites par le programme `diffseq`);

- le fichier [`nc_045512.2_ORF.1.png`](./nc_045512.2_ORF.1.png), qui
  montre les ORF de la séquence [`NC_045512.2`](../data/nc_045512.2.fasta).
  Ce fichier est le résultat du programme `plotorf`;

- le fichier [`nc_045512.2.showorf`](./nc_045512.2.showorf), qui
  correspond au calcul des ORF sur le brin principal de la séquence
  [`NC_045512.2`](../data/nc_045512.2.fasta), obtenu à partir du programme
  `showorf`;

- le fichier [`nc_045512.2_ORF.fasta`](./nc_045512.2_ORF.fasta), qui
  contient les ORF valides de la séquence
  [`NC_045512.2`](../data/nc_045512.2.fasta). Ce fichier est le résultat du programme
  `getorf`.

Les résultats présentés dans le second article sont (dans l'ordre
de présentation dans l'article) :

- le fichier [`nc_045512.2_ORF.csv`](./nc_045512.2_ORF.csv), qui est
  contient tous les ORF identifiés dans le fichier
  [`nc_045512.2_ORF.fasta`](./nc_045512.2_ORF.fasta) au format CSV (avec la
  tabulation pour séparateur) à trois colonnes (l'identifiant, la position
  de début et la position de fin);

- le fichier [`nc_045512.2_ORF.gff`](./nc_045512.2_ORF.gff), qui résulte de
  l'application du script [`csv2gff_ORF.awk`](../scripts/csv2gff_ORF.awk)
  sur le fichier [`nc_045512.2_ORF.csv`](./nc_045512.2_ORF.csv);

- le fichier
  [`nc_045512.2_ORF_distribution.png`](./nc_045512.2_ORF_distribution.png), qui
  résulte de l'application du script
  [`ORF_length_histogram.gp`](./ORF_length_histogram.gp) sur le fichier
  [`nc_045512.2_ORF.csv`](./nc_045512.2_ORF.csv);

- le fichier
  [`nc_045512.2_ORF_filtered.gff`](./nc_045512.2_ORF_filtered.gff), qui
  résulte de l'application du script [`filtre_ORF.awk`](./filtre_ORF.awk)
  sur le fichier [`nc_045512.2_ORF.csv`](./nc_045512.2_ORF.csv).

Les résultats présentés dans le troisième article sont (dans l'ordre
de présentation dans l'article) :

- le fichier [`nc_045512.2.gff`](./nc_045512.2.gff), qui correspond au
  fichier d'annotation final produit pour la séquence de référence du
  SARS-CoV-2 ([`NC_045512.2`](../data/nc_045512.2.fasta));

- le fichier
  [`nc_045512.2_ORF_filtered.csv`](./nc_045512.2_ORF_filtered.csv), qui
  correspond aux identifiants, longueurs des séquences nucléiques et des
  séquences d'acides aminées des ORF qui n'ont pas été retrouvés avec
  `blast`.

- le fichier [`nc_045512.2.embl`](./nc_045512.2.embl), qui correspond à la
  séquence de référence su SARS-CoV-2 avec les annotations. Cette séquence
  est au fomat EMBL et a été produite à partir des fichiers
  [`../data/nc_045512.2.fasta`](../data/nc_045512.2.fasta) et
  [`nc_045512.2.gff`](./nc_045512.2.gff);

- le fichier [`nc_045512.2.showfeat`](./nc_045512.2.showfeat), qui
  correspond à la représentation graphique (ASCII art) des annotations de la
  séquence de référence du SARS-CoV-2
  ([`NC_045512.2`](../data/nc_045512.2.fasta)).

-------------------------------------------------------------------------

Copyright © 2021-2022 -- DoccY's Production

[![Licence Creative Commons](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)

Cette œuvre est mise à disposition selon les termes de la
[Licence Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/).

This work is licensed under a
[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
