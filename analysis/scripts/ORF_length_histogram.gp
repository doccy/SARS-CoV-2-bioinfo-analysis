#!/usr/bin/gnuplot

reset

# See http://www.gnuplotting.org/calculating-histograms/
binwidth = 100
binstart = 0
binval(x) = binwidth*(floor((x-binstart)/binwidth)+0.5)+binstart

set xrange [binstart:*]
set boxwidth 0.9 * binwidth
set style fill solid 0.5
set title "Distribution des longueurs des ORF"
set xlabel "Longueur"
set ylabel "Nombre"

# Par défaut les graphiques sont affichés.
# On change le terminal pour produire des images au format png.
set terminal png notransparent interlace truecolor enhanced nocrop font "Arial,18" size 1600,1200
set output "results/nc_045512.2_ORF_distribution.png"

plot "results/nc_045512.2_ORF.csv" using (binval($3-$2+1)):(1.0) smooth frequency with boxes notitle

