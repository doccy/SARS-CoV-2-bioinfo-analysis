#!/usr/bin/awk --exec

# Assigne simplement le nom du programme à la variable PROGRAM_NAME.
function setProgramName() {
  PROGRAM_NAME = gensub(/^(.*\/)?([^\/]+)\.[^\.]+$/, "\\2", "g", ENVIRON["_"])
  if (!PROGRAM_NAME) {
    PROGRAM_NAME = gensub(/^(.*\/)?([^\/]+)$/, "\\2", "g", ENVIRON["_"])
  }
  PROGRAM_NAME = gensub(/^(.*\/)?([^\/]+)(\.awk)$/, "\\2", "g", ENVIRON["_"])
  if (!PROGRAM_NAME) {
    PROGRAM_NAME = "filtre_ORF"
  }
}

# Initialise les parametres par défaut du programme.
function setDefaultParameters() {
  SEUIL_TAILLE = 150
}

# Calcule la taille d'une ORF (tableau dont les clés "deb" et "fin"
# définissent les positions de début et de fin de l'ORF).
function taille(orf) {
  return orf["fin"] - orf["deb"] + 1
}

# Renvoie une chaine descriptive de l'ORF (tableau dont les clés "id",
# "deb" et "fin" définissent respectivement l'identifiant ainsi que
# les positions de début et de fin d l'ORF).
function orf2string(orf) {
  return orf["id"] "\t" orf["deb"] "\t" orf["fin"]
}

# Rempli le paramètre orf (deuxième argument) à partir de la chaine
# str (premier argument) descriptive de l'ORF. Le tableau résultant
# disposera des clés "id", "deb" et "fin" définissant respectivement
# l'identifiant ainsi que les positions de début et de fin d l'ORF).
function string2orf(str, orf,    tmp) {
  split(str, tmp)
  orf["id"] = tmp[1]
  orf["deb"] = tmp[2]
  orf["fin"] = tmp[3]
}
 
# Renvoie vrai si la première ORF (orf1) inclue intégralement la
# seconde (orf2).
function estInclus(orf1, orf2) {
  return ((orf1["deb"] <= orf2["deb"]) && (orf2["fin"] <= orf1["fin"]))
}

# Compare les longueurs des deux ORF v1 et v2 (respectivement aux
# positions i1 et i2 d'un tableau). Si v1 est plus petite que v2 alors
# la fonction renvoie 1, si v2 est plus grande que v2 alors la
# fonction renvoie -1, si elles sont de même longueur alors la
# fonction renvoie 0. Cette fonction permet de trier les ORF par
# tailles décroissantes.
function length_cmp_fct(i1, v1, i2, v2) {
  l1 = taille(v1)
  l2 = taille(v2)
  return ((l1 < l2) ? 1 : ((l1 > l2) ? -1 : 0))
}

# Compare les positions de départ de deux ORF v1 et v2 (respectivement
# aux positions i1 et i2 d'un tableau). Si v1 débute avant v2 alors la
# fonction renvoie -1, si v1 débute après v2, alors la fonction
# renvoie 1, si elles débutent à la même position, c'est la fonction
# length_cmp_fct qui est appelée. Cette fonction permet de trier les
# ORF par positions croissantes.
function pos_cmp_fct(i1, v1, i2, v2) {
  return ((v1["deb"] < v2["dev"]) ? -1 : ((v1["deb"] > v2["deb"]) ? 1 : length_cmp_fct(i1, v1, i2, v2)))
}

# Supprime les petites ORF incluses dans des plus grandes ORF.  Les
# ORF à analyser son fournies dans le tableau tab1 et les ORF
# conservées sont fournies dans le tableau tab2 qui est trié selon la
# fonction pos_cmp_fct.
function supprime_petits_ORF_inclus(tab1, tab2) {
  delete tmp1
  # Tri par longueurs décroissantes
  asort(tab1,tmp1,"length_cmp_fct")
  for (i = 1; i <= nb; ++i) {
    if (isarray(tmp1[i])) {
      for (j = i + 1; j <= nb; ++j) {
        if (isarray(tmp1[j]) && (taille(tmp1[j]) < SEUIL_TAILLE) && estInclus(tmp1[i], tmp1[j])) {
          delete tmp1[j]
        }
      }
    }
  }
  delete tmp2
  nb = 0
  for (e in tmp1) {
    if (isarray(tmp1[e])) {
      delete tmp2[e][0]
      tmp2[e]["id"] = tmp1[e]["id"]
      tmp2[e]["deb"] = tmp1[e]["deb"]
      tmp2[e]["fin"] = tmp1[e]["fin"]
      ++nb
    }
  }
  delete tab2
  asort(tmp2,tab2,"pos_cmp_fct")
}
 
# Initialisation effectuée au démarrage du script
BEGIN {
  setProgramName()
  setDefaultParameters()
}

# Initialisation effectuée à l'ouverture de chaque fichier passé en
# paramètre
BEGINFILE {
  nb = 0
  name = gensub(/^.*\/([^\/]*)_ORF\.[^\.]*$/, "\\1", "g", FILENAME)
  if (!name) {
    name = gensub(/^.*\/([^\/]*)\.[^\.]*$/, "\\1", "g", FILENAME)
  }
  if (!name) {
    name = FILENAME
  }
  max_pos = 1
}

# Traitement à effectuer pour une ligne du fichier en cours.
{
  ORF[nb]["id"] = $1
  ORF[nb]["deb"] = $2
  ORF[nb]["fin"] = $3 + 3
  ++nb
  if ($3 > max_pos) {
    max_pos = $3
  }
}

# Traitement à effectuer à la fin du fichier en cours.
ENDFILE {
  delete RES
  supprime_petits_ORF_inclus(ORF, RES)
  print "##gff-version 3"
  print "##sequence-region " name " 1 " max_pos
  OFS = "\t"
  for (e in RES) {
    print name, PROGRAM_NAME, "ORF", RES[e]["deb"], RES[e]["fin"], RES[e]["fin"] - RES[e]["deb"] + 1, "+", ((RES[e]["deb"] - 1) % 3), "ID=" name "_" RES[e]["id"] ";name=ORF_" RES[e]["id"]
  }
}
