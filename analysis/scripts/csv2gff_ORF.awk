#!/usr/bin/awk --exec

# Assigne simplement le nom du programme à la variable PROGRAM_NAME.
function setProgramName() {
  PROGRAM_NAME = gensub(/^(.*\/)?([^\/]+)\.[^\.]+$/, "\\2", "g", ENVIRON["_"])
  if (!PROGRAM_NAME) {
    PROGRAM_NAME = gensub(/^(.*\/)?([^\/]+)$/, "\\2", "g", ENVIRON["_"])
  }
  PROGRAM_NAME = gensub(/^(.*\/)?([^\/]+)(\.awk)$/, "\\2", "g", ENVIRON["_"])
  if (!PROGRAM_NAME) {
    PROGRAM_NAME = "filtre_ORF"
  }
}

# Calcule la taille d'une ORF (tableau dont les clés "deb" et "fin"
# définissent les positions de début et de fin de l'ORF).
function taille(orf) {
  return orf["fin"] - orf["deb"] + 1
}

# Renvoie une chaine descriptive de l'ORF (tableau dont les clés "id",
# "deb" et "fin" définissent respectivement l'identifiant ainsi que
# les positions de début et de fin d l'ORF).
function orf2string(orf) {
  return orf["id"] "\t" orf["deb"] "\t" orf["fin"]
}

# Rempli le paramètre orf (deuxième argument) à partir de la chaine
# str (premier argument) descriptive de l'ORF. Le tableau résultant
# disposera des clés "id", "deb" et "fin" définissant respectivement
# l'identifiant ainsi que les positions de début et de fin d l'ORF).
function string2orf(str, orf,    tmp) {
  split(str, tmp)
  orf["id"] = tmp[1]
  orf["deb"] = tmp[2]
  orf["fin"] = tmp[3]
}


# Initialisation effectuée au démarrage du script
BEGIN {
  setProgramName()
}

# Initialisation effectuée à l'ouverture de chaque fichier passé en
# paramètre
BEGINFILE {
  nb = 0
  name = gensub(/^.*\/([^\/]*)_ORF\.[^\.]*$/, "\\1", "g", FILENAME)
  if (!name) {
    name = gensub(/^.*\/([^\/]*)\.[^\.]*$/, "\\1", "g", FILENAME)
  }
  if (!name) {
    name = FILENAME
  }
  max_pos = 1
}

# Traitement à effectuer pour une ligne du fichier en cours.
{
  ORF[nb]["id"] = $1
  ORF[nb]["deb"] = $2
  ORF[nb]["fin"] = $3 + 3
  ++nb
  if ($3 > max_pos) {
    max_pos = $3
  }
}

# Traitement à effectuer à la fin du fichier en cours.
ENDFILE {
  print "##gff-version 3"
  print "##sequence-region " name " 1 " max_pos
  OFS = "\t"
  for (e in ORF) {
    print name, PROGRAM_NAME, "ORF", ORF[e]["deb"], ORF[e]["fin"], ORF[e]["fin"] - ORF[e]["deb"] + 1, "+", ((ORF[e]["deb"] - 1) % 3), "ID=" name "_" ORF[e]["id"] ";name=ORF_" ORF[e]["id"]
  }
}
