---
title: Initiation à la bioinformatique

subtitle: Scripts

author: Alban Mancheron

lang: fr-FR

---

Ce répertoire contient les scripts développés en vue d'analyser les [données de
SARS-CoV-2](../data/) dans le cadre de la série d'articles d'initiation à la
bioinformatique soumis à la revue GNU/Linux Magazine France.

Aucun script n'a été développé dans le cadre du premier article.

Dans le cadre du second article, les scripts suivants ont été développés:

- le script [`csv2gff_ORF.awk`](./csv2gff_ORF.awk), qui prend un fichier au
  format CSV (avec la tabulation pour séparateur) à trois colonnes (un
  identifiant, une position de début et un position de fin) et qui le
  transforme en fichier GFF;

- le script [`filtre_ORF.awk`](./filtre_ORF.awk), qui est une amlioration
  du script précédent et qui prend également un fichier au format CSV (avec
  la tabulation pour séparateur) à trois colonnes (un identifiant, une
  position de début et un position de fin) et qui le transforme en fichier
  GFF en ne conservant que les ORF qui ne sont pas inclus dans un ORF plus
  grand ou bien qui sont d'une taille significative;

- le script [`ORF_length_histogram.gp`](./ORF_length_histogram.gp), qui
  génère l'histogramme des distributions des longueurs des ORF à partir d'un
  fichier CSV (avec la tabulation pour séparateur) à trois colonnes (un
  identifiant, une position de début et un position de fin).

Dans le cadre du troisième article, le scripts suivant a été développé:

- le script [`brutal_analysis.sh`](./brutal_analysis.sh) permet
  d'automatiser l'analyse d'une région d'intérêt. Ce script prend
  l'identifiant de l'ORF à analyser et nécessite d'avoir un fichier
  `sequences.gb` (contenant les séquences similaires) dans le répertoire
  `/tmp`.

-------------------------------------------------------------------------

Copyright © 2021-2022 -- DoccY's Production

[![Licence Creative Commons](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)

Cette œuvre est mise à disposition selon les termes de la
[Licence Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/).

This work is licensed under a
[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
