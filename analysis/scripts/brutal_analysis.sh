#!/bin/bash

set -e

WORKING_DIR="$(dirname "$0")"
test -n "${WORKING_DIR}"

PROGNAME=$(basename "$0")
test -n "${PROGNAME}"

cd "${WORKING_DIR}"
if test ! -f ${PROGNAME}; then
  echo "BUGGGG"
  exit 1
fi

ORF_ID="$1"
if test -z "${ORF_ID}"; then
  cat <<EOF
usage: ${PROGNAME} <ID>
avec <ID> l'identifiant de l'ORF à analyser
EOF
  exit 1
fi

cd ../
orgn="nc_045512.2"
res_dir="results"
tmp_dir="${res_dir}/tmp_${ORF_ID}"
fich_base="${res_dir}/ORF/${orgn}"
fich="${fich_base}_${ORF_ID}"
fich_tpl="${fich}_related"

echo "Preparation du répertoire de travail"
rm -rf "${tmp_dir}"
mkdir "${tmp_dir}"

echo "Récupération des données au format genbank"
mv /tmp/sequence.gb ${fich_tpl}.gb

echo "Extraction des séquences"
seqretsplit ${fich_tpl}.gb -feature -osdirectory2 ${tmp_dir} -osformat2 fasta -auto
echo "Extraction des annotations"
seqretsplit ${fich_tpl}.gb -feature -osdirectory2 ${tmp_dir} -osformat2 gff -auto -stdout

echo "Construction des séquences à aligner"
cat ${fich}.fasta ${tmp_dir}/*fasta > ${fich_tpl}.fasta

echo "Alignement des séquences"
emma ${fich_tpl}.fasta -outseq ${fich_tpl}.emma -dendoutfile ${fich_tpl}.dnd -osformat2 msf -auto

echo "Visualisation du dendrogramme"
newicktotxt ${fich_tpl}.dnd
mv ${fich_base%.*}.txt ${fich_tpl}.txt
newicktops ${fich_tpl}.dnd
convert ${fich_base%.*}.ps ${fich_tpl}.png
rm -f ${fich_base%.*}.ps
eog ${fich_tpl}.png || xdg-open ${fich_tpl}.png

echo "Création du graphique de conservation"
plotcon ${fich_tpl}.emma -graph png -goutfile ${fich_tpl}_plotcon -auto

echo "Visualisation du graphique de conservation"
eog ${fich_tpl}_plotcon.1.png || xdg-open ${fich_tpl}_plotcon.1.png

echo "Visualisation de l'alignement"
less ${fich_tpl}.emma

echo "Édition des annotations pour l'ORF ${ORF_ID}"
joe ${res_dir}/${orgn}.gff ${tmp_dir}/*.gff 

echo "That's all Folks\!\!\!"
