---
title: Initiation à la bioinformatique

subtitle: Analyses

author: Alban Mancheron

lang: fr-FR

---

Ce répertoire contient les analyses effectuées dans le cadre de la série
d'articles d'initiation à la bioinformatique soumis à la revue GNU/Linux
Magazine France.

Le sous-repertoire [`data`](./data) contient les données analysées dans la
série d'articles.

Le sous-repertoire [`scripts`](./scripts) contient l'ensemble des scripts
d'analyse créés pour les besoins de l'analsye et présentés en substance dans
la série d'articles.

Le sous-repertoire [`results`](./results) contient l'ensemble des résultats
d'analyse obtenus et présentés en substance dans la série d'articles.

-------------------------------------------------------------------------

Copyright © 2021-2022 -- DoccY's Production

[![Licence Creative Commons](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)

Cette œuvre est mise à disposition selon les termes de la
[Licence Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/).

This work is licensed under a
[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
