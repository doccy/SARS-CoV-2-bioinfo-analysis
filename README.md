---
title: Initiation à la bioinformatique

author: Alban Mancheron

lang: fr-FR

---

Ce dépôt correspond au matériel associé à la série d'articles d'initiation à
la bioinformatique soumis à la revue GNU/Linux Magazine France.

Le fichier [`IntroBioinfo_1.odt`](./IntroBioinfo_1.odt) correspond au
premier article de la série.

Le fichier [`IntroBioinfo_2.odt`](./IntroBioinfo_2.odt) correspond au
second article de la série.

Le fichier [`IntroBioinfo_3.odt`](./IntroBioinfo_3.odt) correspond au
second article de la série.

Le sous-répertoire [`analysis/`](analysis/) contient toutes les données, les
scripts et les résultats utilisés pour la série d'articles.

Le sous-répertoire [`images/`](images/) contient les images associés aux
articles soumis.

-------------------------------------------------------------------------

Copyright © 2021-2022 -- DoccY's Production

[![Licence Creative Commons](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)

Cette œuvre est mise à disposition selon les termes de la
[Licence Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/).

This work is licensed under a
[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
