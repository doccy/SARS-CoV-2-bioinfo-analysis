---
title: Initiation à la bioinformatique

subtitle: Images du premier article

author: Alban Mancheron

lang: fr-FR

---

Ce répertoire contient les images associées au premier article de la série
d'initiation à la bioinformatique soumis à la revue GNU/Linux Magazine
France.

Il s'agit, dans l'ordre d'appartition dans l'article :

- de l'image
  [`NCBI_COVID-19_Information.png`](./NCBI_COVID-19_Information.png),
  montrant le bandeau spécial COVID-19 apparaissant sur le site /web/ du
  [NCBI](https://www.ncbi.nlm.nih.gov/);

- de l'image
  [`NCBI_Explore_Virus_Hub.png`](./NCBI_Explore_Virus_Hub.png),
  montrant le bouton permettant (entre autres) de récupérer les données de
  SARS-CoV-2 sur le site /web/ du [NCBI](https://www.ncbi.nlm.nih.gov/);

- de l'image [`MRNA_structure_fr.svg.png`](./MRNA_structure_fr.svg.png),
  copie du fichier
  https://commons.wikimedia.org/wiki/File:MRNA_structure_fr.svg
  faisant partie du domaine public.

- de l'image [`nc_045512.2_ORF.1.png`](./nc_045512.2_ORF.1.png), qui a été
  produite par l'outil `plotorf` à partir de la séquence
  [`NC_045512.2`](../data/nc_045512.2.fasta).

-------------------------------------------------------------------------

Copyright © 2021-2022 -- DoccY's Production

[![Licence Creative Commons](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)

Cette œuvre est mise à disposition selon les termes de la
[Licence Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/).

This work is licensed under a
[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
