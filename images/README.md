---
title: Initiation à la bioinformatique

subtitle: Images

author: Alban Mancheron

lang: fr-FR

---

Ce répertoire contient les images associées aux différents articles écrits
dans le cadre de la série d'articles d'initiation à la bioinformatique pour
la revue GNU/Linux Magazine France.

Le sous-repertoire [`IntroBioinfo_1`](./IntroBioinfo_1) contient les images
relatives au premier article de la série.

Le sous-repertoire [`IntroBioinfo_2`](./IntroBioinfo_2) contient les images
relatives au second article de la série.

Le sous-repertoire [`IntroBioinfo_3`](./IntroBioinfo_3) contient les images
relatives au troisième article de la série.

-------------------------------------------------------------------------

Copyright © 2021-2022 -- DoccY's Production

[![Licence Creative Commons](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)

Cette œuvre est mise à disposition selon les termes de la
[Licence Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/).

This work is licensed under a
[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
