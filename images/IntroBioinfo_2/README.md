---
title: Initiation à la bioinformatique

subtitle: Images du second article

author: Alban Mancheron

lang: fr-FR

---

Ce répertoire contient les images associées au second article de la série
d'initiation à la bioinformatique soumis à la revue GNU/Linux Magazine
France.

Il s'agit, dans l'ordre d'appartition dans l'article :

- de l'image [`nc_045512.2_ORF.gff.png`](./nc_045512.2_ORF.gff.png), qui
  correspond à la cartographie des ORF trouvés pour la séquence de référence
  du SARS-CoV-2 [NC_045512.2](../../analysis/data/nc_045512.2.fasta) réalisée par le
  programme
  [`Annotation Sketch`](http://genometools.org/annotationsketch.html) de la
  suite [GenomeTools](http://genometools.org/);

- de l'image
  [`nc_045512.2_ORF_distribution.png`](./nc_045512.2_ORF_distribution.png),
  qui correspond à l'histogramme des distributions des longueurs des ORF
  valides trouvées pour la séquence de référence du SARS-CoV-2
  [NC_045512.2](../data/nc_045512.2.fasta) réalisée avec le script
  [`ORF_length_histogram.gp`](../../analysis/scripts/ORF_length_histogram.gp);

- de l'image
  [`nc_045512.2_ORF_filtered.gff.png`](./nc_045512.2_ORF_filtered.gff.png),
  qui correspond à la cartographie des ORF (après filtrage) trouvés pour la
  séquence de référence du SARS-CoV-2
  [NC_045512.2](../../analysis/data/nc_045512.2.fasta) réalisée par le
  programme [`Annotation Sketch`](http://genometools.org/annotationsketch.html)
  de la suite [GenomeTools](http://genometools.org/);

- de l'image [`NCBI_BLAST_button.png`](./NCBI_BLAST_button.png), qui est une
  capture d'écran du bouton permettant d'accéder au programme `blast` pour
  les protéines sur le site du [NCBI](https://www.ncbi.nlm.nih.gov/);

- de l'image [`NCBI_BLAST_submission.png`](./NCBI_BLAST_submission.png), qui
  est une capture d'écran du formulaire de soumission au programme `blast`
  (pour les protéines) de la chaîne d'acides aminés traduite étudiée
  (correspondante à
  l'[ORF 81](../../analysis/results/ORF/nc_045512.2_81.fasta)) sur le site
  du [NCBI](https://www.ncbi.nlm.nih.gov/);

- de l'image [`NCBI_BLAST_filter.png`](./NCBI_BLAST_filter.png), qui est une
  capture d'écran de la zone permettant de restreindre les résultats
  affichés par le programme `blast` (pour les protéines) sur le site du
  [NCBI](https://www.ncbi.nlm.nih.gov/);

- de l'image [`NCBI_BLAST_result_list.png`](./NCBI_BLAST_result_list.png),
  qui est une capture d'écran des résultats produits par le programme
  `blast` (pour les protéines) pour la chaîne d'acides aminés traduite
  étudiée (correspondante à
  l'[ORF 81](../../analysis/results/ORF/nc_045512.2_81.fasta)) sur le site
  du [NCBI](https://www.ncbi.nlm.nih.gov/);

- de l'image
  [`NCBI_BLAST_results_graphic_summary.png`](./NCBI_BLAST_results_graphic_summary.png),
  qui est une capture d'écran de la vue graphique des résultats produits par
  le programme `blast` (pour les protéines) pour la chaîne d'acides aminés
  traduite étudiée (correspondante à
  l'[ORF 81](../../analysis/results/ORF/nc_045512.2_81.fasta)) sur le site
  du [NCBI](https://www.ncbi.nlm.nih.gov/);

- de l'image
  [`NCBI_BLAST_results_graphic_summary_detail.png`](./NCBI_BLAST_results_graphic_summary_detail.png),
  qui est une capture d'écran du détail des projections des informations
  issues des séquences alignées par le programme `blast` (pour les
  protéines) pour la chaîne d'acides aminés traduite étudiée (correspondante
  à l'[ORF 81](../../analysis/results/ORF/nc_045512.2_81.fasta) sur le site du
  [NCBI](https://www.ncbi.nlm.nih.gov/);

- de l'image
  [`nc_045512.2_81_related-treeviewx.png`](./nc_045512.2_81_related-treeviewx.png),
  qui correspond à l'image générée par l'outil
  [`treeviewx`](https://github.com/rdmpage/treeviewx) à partir de l'arbre de
  guidage obtenu en sortie de l'application du programme
  [ClustalW](http://www.clustal.org/omega/) sur les 28 séquences présentes dans
  l'arbre.

-------------------------------------------------------------------------

Copyright © 2021-2022 -- DoccY's Production

[![Licence Creative Commons](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)

Cette œuvre est mise à disposition selon les termes de la
[Licence Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/).

This work is licensed under a
[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
